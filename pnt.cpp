#include <phoenix/phoenix.hpp>
#include <nall/stdint.hpp>
#include <stdio.h>

using namespace nall;
using namespace phoenix;

void setPixel(image * buffer, int x, int y, uint32_t color);


struct WorkingState
{
    image * buffer;
    Canvas * target;
} WorkingState;

void canvasMouseMove(Position mouse)
{
    printf("Mouse moved: %d, %d\n", mouse.x, mouse.y);
    setPixel(WorkingState.buffer, mouse.x, mouse.y, 0xFF000000);
    WorkingState.target->setImage(*WorkingState.buffer); // Needs to be moved somewhere with vsync later
}


struct MainWindow : Window
{
    MainWindow();
    void reSetCanvas();
    VerticalLayout layout;
    Canvas canvas;
    image buffer;
};
MainWindow::MainWindow()
{
    setFrameGeometry({64, 64, 640, 480});
    
    WorkingState.buffer = &buffer;
    WorkingState.target = &canvas;
    canvas.onMouseMove = canvasMouseMove;

    layout.setMargin(5);
    layout.append(canvas, {288, 360});
    append(layout);

    canvas.setSize({288, 360});
    buffer.allocate(288, 360);
    buffer.fill(0xFFFFFFFF);
    setPixel(&buffer, 10, 10, 0xFF000000);
    setPixel(&buffer, 11, 10, 0xFF000000);
    setPixel(&buffer, 12, 10, 0xFF000000);
    setPixel(&buffer, 11, 11, 0xFF000000);
    setPixel(&buffer, 12, 11, 0xFF000000);
    setPixel(&buffer, 12, 12, 0xFF000000);
    
    canvas.setImage(buffer);
    
    onClose = &Application::quit;

    setVisible();
}

void setPixel(image * buffer, int x, int y, uint32_t color)
{
    if (y > buffer->height or x > buffer->width)
        return;
    uint8_t * addr = buffer->data;
    addr += y * buffer->width * 4;
    addr += x * 4;
    buffer->write(addr, color);
}

int main() {
    new MainWindow;
    Application::run();
    return 0;
}
